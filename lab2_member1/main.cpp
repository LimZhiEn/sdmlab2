#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

void Fahrenheit(float celsius, float fahrenheit);
void Celsius(float fahrenheit, float celsius);
void Circumference(float radius, float PI, float circumference);
void Area(float radius, float PI, float area);
void AreaR(float length, float width, float area);
void AreaT(float sideA, float sideB, float sideC, float area, float s);
void VolumeCylinder(float radius, float height, float PI, float volume);
void VolumeCone(float radius, float height, float PI, float volume);


int mainmenu;
bool valid = 0;
float celsius, fahrenheit;
float PI = 3.14159265359;
float radius, circumference, area;
float length,height, width, base;
float volume;
float sideA, sideB, sideC,s;

int main()
{
    do
    {
        valid = 1;
        cout << "\n------------------------------------------" << endl;
        cout << "|                Main Menu               |" << endl;
        cout << "------------------------------------------" << endl;
        cout << "| 1. Convert Celsius to Fahrenheit       |" << endl;
        cout << "| 2. Convert Fahrenheit to Celsius       |" << endl;
        cout << "| 3. Calculate Circumference of a circle |" << endl;
        cout << "| 4. Calculate Area of a circle          |" << endl;
        cout << "| 5. Area of Rectangle                   |" << endl;
        cout << "| 6. Area of Triangle (Heron's Formula)  |" << endl;
        cout << "| 7. Volume of Cylinder                  |" << endl;
        cout << "| 8. Volume of Cone                      |" << endl;
        cout << "| 9. Quit Program                        |" << endl;
        cout << "------------------------------------------" << endl;

        cout << " Select Any [1 - 9] From Menu Above => ";
        cin >> mainmenu;
        cout << endl;

        if (cin.fail())
        {
            cout << " Error Was Found | Numeric Value Only" << endl << endl;
            cin.clear();
            cin.ignore(999, '\n');
        }

        switch (mainmenu)
        {
        case 1:
        {
            Fahrenheit(celsius, fahrenheit);
            break;
        }
        case 2:
        {
            Celsius(fahrenheit, celsius);
            break;
        }
        case 3:
        {
            Circumference(radius, PI, circumference);
            break;
        }
        case 4:
        {
            Area(radius, PI, area);
            break;
        }
        case 5:
        {
            AreaR(length, width, area);
            break;
        }
        case 6:
        {
            AreaT(sideA, sideB, sideC, s,area);
            break;
        }
        case 7:
        {
            VolumeCylinder(radius, height, PI, volume);
            break;
        }
        case 8:
        {
            VolumeCone(radius, height, PI, volume);
            break;
        }
        case 9:
        {
            cout << "Bye.";
            break;
        }
        }
    }
    while (mainmenu != 9);
    return 0;
}

//Convert Celsius to Fahrenheit
void Fahrenheit(float celsius, float fahrenheit)
{
    bool valid = 0;
    do
    {
        valid = 0;
        cout << " Enter Temperature In Degree Celsius : ";
        cin >> celsius;
        if (cin.fail())
        {
            cout << " Error Was Found | Numeric Value Only" << endl << endl;
            cin.clear();
            cin.ignore(999, '\n');
            valid = 1;
        }
    } while (valid);
    fahrenheit = (celsius * 9 / 5) + 32;
    cout << fixed << setprecision(1);
    cout << " Fahrenheit = " << fahrenheit << endl;
    cout << endl;
}


//Convert Fahrenheit to Celsius
void Celsius(float fahrenheit, float celsius)
{
    bool valid = 0;
    do
    {
        valid = 0;
        cout << " Enter Temperature In Degree Fahrenheit : ";
        cin >> fahrenheit;
        if (cin.fail())
        {
            cout << " Error Was Found | Numeric Value Only" << endl << endl;
            cin.clear();
            cin.ignore(999, '\n');
            valid = 1;
        }
    } while (valid);
    celsius = (fahrenheit - 32) * 5 / 9;
    cout << fixed << setprecision(1);
    cout << " Celsius = " << celsius << endl;
    cout << endl;
}


//Calculate Circumference of a circle
void Circumference(float radius, float PI, float circumference)
{
    bool valid = 0;
    do
    {
        valid = 0;
        cout << " Enter Radius Of The Circle : ";
        cin >> radius;
        if (cin.fail())
        {
            cout << " Error Was Found | Numeric Value Only" << endl << endl;
            cin.clear();
            cin.ignore(999, '\n');
            valid = 1;
        }
        else if (radius <= 1)
        {
            cout << " Error Was Found | Radius Must Be Greater Than 1" << endl << endl;
        }
    } while (valid || radius <= 1);

    circumference = 2 * PI * radius;
    cout << fixed << setprecision(2);
    cout << " Circumference = " << circumference << endl;
    cout << endl;
}

//Calculate Area of a circle
void Area(float radius, float PI, float area)
{
    bool valid = 0;
    do
    {
        valid = 0;
        cout << " Enter Radius Of The Circle : ";
        cin >> radius;
        if (cin.fail())
        {
            cout << " Error Was Found | Numeric Value Only" << endl << endl;
            cin.clear();
            cin.ignore(999, '\n');
            valid = 1;
        }
        else if (radius <= 1)
        {
            cout << " Error Was Found | Radius Must Be Greater Than 1" << endl << endl;
        }
    } while (valid || radius <= 1);

    area = PI * radius * radius;
    cout << fixed << setprecision(2);
    cout << " Area = " << area << endl;
    cout << endl;
}

//Calculate Area Of Rectangle
void AreaR(float length, float width, float area)
{
	bool valid = 0;
	do
	{
		valid = 0;
		cout << " Enter Length Of The Rectangle => ";
		cin >> length;
		if (cin.fail())
		{
			cout << " Error Was Found | Numeric Value Only" << endl << endl;
			cin.clear();
			cin.ignore(999, '\n');
			valid = 1;
		}
		else if (length <= 1)
		{
			cout << " Error Was Found | Length must be greater than 1" << endl << endl;
		}
	} while (valid || length <= 1);

	do
	{
		valid = 0;
		cout << " Enter Width Of The Rectangle => ";
		cin >> width;
		if (cin.fail())
		{
			cout << " Error Was Found | Numeric Value Only" << endl << endl;
			cin.clear();
			cin.ignore(999, '\n');
			valid = 1;
		}
		else if (width <= 1)
		{
			cout << " Error Was Found | Width must be greater than 1" << endl << endl;
		}
	} while (valid || width <= 1);

	area = length * width;
	cout << fixed << setprecision(2);
	cout << " Area = " << area << endl;
	cout << endl;
}
//Calculate Area Of Triangle
void AreaT(float sideA, float sideB, float sideC, float area, float s)
{
	bool valid = 0;
	do
	{
		valid = 0;
		cout << " Enter side A Of The Triangle => ";
		cin >> sideA;
		if (cin.fail())
		{
			cout << " Error Was Found | Numeric Value Only" << endl << endl;
			cin.clear();
			cin.ignore(999, '\n');
			valid = 1;
		}
		else if (sideA <= 1)
		{
			cout << " Error Was Found | Number must be greater than 1" << endl << endl;
		}
	} while (valid || sideA <= 1);

	do
	{
		valid = 0;
		cout << " Enter side B Of The Triangle => ";
		cin >> sideB;
		if (cin.fail())
		{
			cout << " Error Was Found | Numeric Value Only" << endl << endl;
			cin.clear();
			cin.ignore(999, '\n');
			valid = 1;
		}
		else if (sideB <= 1)
		{
			cout << " Error Was Found | Number must be greater than 1" << endl << endl;
		}
		}while (valid || sideB <= 1);

	do
	{
		valid = 0;
		cout << " Enter side C Of The Triangle => ";
		cin >> sideC;
		if (cin.fail())
		{
			cout << " Error Was Found | Numeric Value Only" << endl << endl;
			cin.clear();
			cin.ignore(999, '\n');
			valid = 1;
		}
		else if (sideC <= 1)
		{
			cout << " Error Was Found | Number must be greater than 1" << endl << endl;
		}
	} while (valid || sideC <= 1);

    //calculate the semi-perimeter of the triangle
    s = (sideA+sideB+sideC)/2;
    //calculate the triangle area
    area = sqrt(s*(s-sideA)*(s-sideB)*(s-sideC));
    cout << " The semi perimeter = " << s << endl;
	cout << fixed << setprecision(2);
	cout << " The Area of Triangle = " << area << endl;
	cout << endl;

}

//Calculate Volume Of Cylinder
void VolumeCylinder(float radius, float height, float PI, float volume)
{
	bool valid = 0;
	do
	{
		valid = 0;
		cout << " Enter radius Of The Cylinder => ";
		cin >> radius;
		if (cin.fail())
		{
			cout << " Error Was Found | Numeric Value Only" << endl << endl;
			cin.clear();
			cin.ignore(999, '\n');
			valid = 1;
		}
		else if (radius <= 1)
		{
			cout << " Error Was Found | Radius must be greater than 1" << endl << endl;
		}
	} while (valid || radius <= 1);

	do
	{
		valid = 0;
		cout << " Enter Height Of The Cylinder => ";
		cin >> height;
		if (cin.fail())
		{
			cout << " Error Was Found | Numeric Value Only" << endl << endl;
			cin.clear();
			cin.ignore(999, '\n');
			valid = 1;
		}
		else if (height <= 1)
		{
			cout << " Error Was Found | Height must be greater than 1" << endl << endl;
		}

	} while (valid || height <= 1);

	volume = PI * radius * radius * height;
	cout << fixed << setprecision(2);
	cout << " Volume = " << volume << endl;
	cout << endl;
}

//Calculate Volume Of Cone
void VolumeCone(float radius, float height, float PI, float volume)
{
	bool valid = 0;
	do
	{
		valid = 0;
		cout << " Enter radius Of The Cone => ";
		cin >> radius;
		if (cin.fail())
		{
			cout << " Error Was Found | Numeric Value Only" << endl << endl;
			cin.clear();
			cin.ignore(999, '\n');
			valid = 1;
		}
		else if (radius <= 1)
		{
			cout << " Error Was Found | Radius must be greater than 1" << endl << endl;
		}
	} while (valid || radius <= 1);

	do
	{
		valid = 0;
		cout << " Enter Height Of The Cone => ";
		cin >> height;
		if (cin.fail())
		{
			cout << " Error Was Found | Numeric Value Only" << endl << endl;
			cin.clear();
			cin.ignore(999, '\n');
			valid = 1;
		}
		else if (height <= 1)
		{
			cout << " Error Was Found | Height must be greater than 1" << endl << endl;
		}
	} while (valid || height <= 1);

	volume = PI * radius * radius * height / 3;
	cout << fixed << setprecision(2);
	cout << " Volume = " << volume << endl;
	cout << endl;
}
